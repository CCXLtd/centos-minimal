#!/bin/bash

CENTOS_VER="7"
CENTOS_TAR="centos${CENTOS_VER}-zero.tar"

vagrant destroy -f

vagrant up && \
vagrant scp :${CENTOS_TAR}  . && \
docker import ${CENTOS_TAR} codered/centos-minimal:latest && \
rm -f ${CENTOS_TAR} && \
vagrant destroy -f

docker tag codered/centos-minimal:latest codered/centos-minimal:${CENTOS_VER}

# no docker push - do it manualy

# docker push codered/centos-minimal:latest
# docker push codered/centos-minimal:latest:${CENTOS_VER}
