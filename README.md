# Minimal Centos Docker Image

This will build a minimal Docker Centos image using [vagrant](https://www.vagrantup.com/) and [supermin](https://github.com/libguestfs/supermin)

## Requirements

* vagrant
* vagrant-scp - vagrant plugin install vagrant-scp


## Build
It is recommended to use a proxy to reduce bandwith usage.
Change the "http_proxy" export in the Vagrantfile to match your environment.

```bash
./build.sh
```

Check that image is present

```bash
docker images | grep centos-minimal
```
